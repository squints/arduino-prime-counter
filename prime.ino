#include <avr/sleep.h>
#include <SPI.h>
#include "Adafruit_FRAM_SPI.h"
//Adafruit's SPI-FRAM headers can be found here:
//https://github.com/adafruit/Adafruit_FRAM_SPI

const int wakePin = 2;    
const int ledPin = 13;
const int counterPin = 3;

const uint8_t FRAM_SCK= 4;
const uint8_t FRAM_MISO = 5;
const uint8_t FRAM_MOSI = 6;
const uint8_t FRAM_CS = 7;
Adafruit_FRAM_SPI fram = Adafruit_FRAM_SPI(FRAM_SCK, FRAM_MISO, FRAM_MOSI, FRAM_CS);
const uint16_t addr = 0;

int sleepStatus = 0; //sleep flag

unsigned long lastPrime;

void wakeUpNow(){} //dummy method

//the next two methods find the next prime, given a number
bool is_prime(unsigned long x)
{
  for (unsigned long i = 3; true; i += 2)
  {
    if ((x / i) < i)
      return true;
    if (x == (x / i) * i)
      return false;
  }
  return true;
}

unsigned long nextPrime(unsigned long x)
{
  if (x <= 2)
    return 2;
  if (!(x & 1))
    ++x;
  for (; !is_prime(x); x += 2)
    ;
  return x;
}

//turns the counter
void counterClick(unsigned long newPrime)
{

  Serial.println("Last prime: ");
  Serial.println(lastPrime);
  Serial.println("New prime: ");
  Serial.println(newPrime);
  Serial.println("Difference: ");
  Serial.println(newPrime-lastPrime);
  Serial.println("Click Check: ");
  for(int x = 0; x < (newPrime-lastPrime); x++)
  {
    digitalWrite(ledPin, HIGH);
    Serial.print("CLICK! ");
    digitalWrite(counterPin, HIGH);
    delay(100);
    digitalWrite(ledPin, LOW);
    digitalWrite(counterPin, LOW);
    delay(250);
  }
  fram.writeEnable(true);
  fram.write(addr, (const uint8_t*) &newPrime, sizeof(long));
  fram.writeEnable(false);
  lastPrime = newPrime;
}

//checks if the counter is about to rollover, to keep sync
void rollOver()
{

  Serial.println("Uh-oh. Rollover!");
  //last prime before 10m = 9999991
  //9 more = 0000000, then two more after 
  for(int x = 0; x < 11; x++)
  {
    digitalWrite(ledPin, HIGH);
    Serial.print("CLICK! ");
    digitalWrite(counterPin, HIGH);
    delay(250);
    digitalWrite(ledPin, LOW);
    digitalWrite(counterPin, LOW);
    delay(750);
  }
  lastPrime = 2;
  fram.writeEnable(true);
  fram.write(addr, (const uint8_t*) &lastPrime, sizeof(long));
  fram.writeEnable(false);

}

void framPanic()
{
    Serial.println(" ***** No SPI FRAM found ... check your connections! ****");
    Serial.println("     * Current PINOUT *");
    Serial.print("SCK : "); Serial.println(FRAM_SCK);
    Serial.print("MISO: "); Serial.println(FRAM_MISO);
    Serial.print("MOSI: "); Serial.println(FRAM_MOSI);
    Serial.print("CS  : "); Serial.println(FRAM_CS);
    Serial.println(" ***** ERROR IN FRAM ****");
    for(int x = 0; x < 120; x++)
    {
      digitalWrite(ledPin, HIGH);
      delay(500);
      digitalWrite(ledPin, LOW);
      delay(500);
    }
    sleepNow();
    setup();
}

void sleepNow()         // here we put the arduino to sleep
{

  set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // sleep mode is set here

  sleep_enable();          
  //wakeUpNow is used as a dummy method. Consider process resuming after sleep_disable
  attachInterrupt(0,wakeUpNow, LOW);// use interrupt 0 (pin 2) and run function
  // wakeUpNow when pin 2 gets LOW 
  Serial.println("Sleeping...");
  delay(250);
  sleep_mode();            // here the device is actually put to sleep!!
  // THE PROGRAM CONTINUES FROM HERE AFTER WAKING UP

  sleep_disable();         // first thing after waking from sleep:
  // disable sleep...
  detachInterrupt(0);     
  delay(50); // disables interrupt 0 on pin 2 so the 
  // wakeUpNow code will not be executed 
  // during normal running time.

}

void setup()
{
  pinMode(wakePin, INPUT_PULLUP);
  digitalWrite (wakePin, HIGH);
  pinMode(ledPin, OUTPUT);
  pinMode(counterPin, OUTPUT);
  if (fram.begin()) {
    Serial.println("FRAM Check OK!");
  } else {
    //UH-OH! There's a problem with the FRAM
    framPanic();
  }
  /** //use in case of resyncing counter
    unsigned long newPrime = 1206; 
    fram.writeEnable(true);
    fram.write(addr, (const uint8_t*) &newPrime, sizeof(long));
    fram.writeEnable(false);
   /**/
  fram.read(addr, (uint8_t*) &lastPrime, sizeof(long));
  Serial.begin(9600);
  attachInterrupt(0, wakeUpNow, LOW); // use interrupt 0 (pin 2) and run function
  sleepNow();                     // wakeUpNow when pin 2 gets LOW 
}

void loop()
{
  Serial.println("Unggg... waking up...");
  unsigned long newPrime = nextPrime(lastPrime+1);
  if(newPrime > 9999999)
  {
    rollOver(); 
  }
  else
  {
    counterClick(newPrime);
  }
  sleepNow(); 
}
//copyleft Mitchell "squints" McCarsky/Oct. 2013,2017. Some snippets borrowed from arduino.cc and StackOverflow forum members
//sketch size: 5,308b ; mem: 474b
//using Mega2560r3+ 